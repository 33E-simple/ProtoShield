## Prototyping Shield

Layout for a prototyping shield for the dsPIC-EL-GM.  Although a
normal Arduino shield will work, a custom shield allows us to use the
maximum real estate available and also is somewhat less expensive than
a purchased shield, even after the connectors are added.

---


![Shield image](shieldL.png)

---

